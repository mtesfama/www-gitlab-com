---
layout: handbook-page-toc
title: "Hire to Retire"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


**Introduction**

Hire to Retire includes the process of recruitment, payroll and separation from the organisation. This page decribes the various stages of the Hire to Retire process.

This process is dependant upon the following applications:

 |Application| Primary use|
 |----|----|
 |Greenhouse| Recruitment process of candidates|
 |BambooHR| Maintains the personnel records of employees and pay structure of the employee|
 |ADP| Payroll processing of US entity employees|
 |Google Sheets|Payroll change report|


## 1.  Recruitment

[Link to flowchart](https://drive.google.com/open?id=1e9u9_0yD7Jedjj2iZwEqfk1s0EiJb8W3scrwJqHaE2U)

**Hiring in Greenhouse**

* GitLab uses Greenhouse for recruitment activities **(H2R.C.01 and H2R.C.03)**. Following are the key activities performed through Greenhouse:

    * Approving new jobs
    * Posting job vacancies internally
    * Reviewing Candidate applications
    * Scheduling interviews
    * Approving offers **(H2R.C.01)**
    * Sending contracts of hire
    * Updating Job information <br><br>

Employee referrals are made through issues based on details mentioned [here](/handbook/hiring/referral-process/).

**Creating a job on Greenhouse**

* Navigate to the [Add icon](https://drive.google.com/open?id=17TUOnNCuRGXPStCqJZ2e71eee17uHFMNqoG9xfvtKto) in the upper right-hand corner and select `Create a Job` from the drop-down menu.

* Click `Copy an Existing Job`, then filter `Job Status = Draft` and select the appropriate [TEMPLATE]. Alternatively, you can use the `Department` filter to narrow the options.

    * From the [subsequent page](https://drive.google.com/open?id=1K8eqlD2TuT0kNte0Q-YnybxREEt_Zt0UKRwtWKDAU3k), select either `Copy an Existing Job`.
    * Select a [TEMPLATE], then proceed as per the Job setup flow configured in the system.
    * Complete the steps of the job setup flow to create the vacancy.
    * Once a vacancy has been selected to copy, the first screen will ask for basic job info.
    * Input the number of openings to be hired for this role.
    * Under "Employment Type", select if the vacancy is a full-time, part-time, intern, or contract role.
    * Under "Salary"
        * For roles in the [Compensation Calculator Tool](/handbook/total-rewards/compensation/compensation-calculator/calculator/): Take the benchmark x level x 0.25 for the low end and benchmark x level x 0.85 on the high end.[Refer benchmark calculation](/handbook/total-rewards/compensation/#exchange-rates)
        * For roles not in the compensation calculator (Sales/Director/Exec): Leave blank if not known. The People Operations Analyst will edit as the first level of approval. People Ops Analysts will pull survey data in San Francisco for this role then apply the same formula for 0.25 on the low end and 0.8 on the high end.
    * If the vacancy is eligible for a bonus, input the range of the bonus amount under the "Bonus" field. If there are no bonuses associated with this vacancy, then the field is to be left blank.
    * If this vacancy is eligible for stock options, input the range of offered stock options under the "Options" field.
    * Under "Type" choose if this is a new hire or a backfill.
    * Click "Create Job & Continue".
    * The next page consists of all of the attributes interviewers will be evaluating for candidates in their scorecards across the full interview process.

**Scorecard updating**

* Scorecards are text boxes where the vacancy creator can add in their notes. Some scorecards for different roles or stages may have additional text boxes for specific questions. If a text box is required, there will be a red asterisk by the question.
* Underneath the first text box "Key Take-Away".
* Further, there are two additional links, Private Note and Note for Other Interviewers, which will open upon clicking.
* Below the text boxes for notes, each role has a list of attributes that we are looking for. Each stage in the process has certain attributes highlighted that it's recommended for evaluation.
* The scorecard will automatically save the entered information.
* Finally, click Submit Scorecard.

**Creation of stages in candidate assessment**

* The next section is the Interview Plan, fill in the Interview Plan accordingly.
* The `Review` stage is mandatory.
* Many vacancies have an `Assessment as the first step in the process.
* If the vacancy requires an `Assessment`, but there is no `Assessment` stage already added, scroll to the bottom of the page and click ["Add a Stage"](https://drive.google.com/open?id=1YNN-Im-QbxK75v-eB7wjou6WmCbzM_r7ZV2QkfWgEjM).
* Click the “Edit” button, then create the stage by filling in the applicable fields, such as “Custom Questions.” In the “[Interview Prep](https://drive.google.com/open?id=1j5W6wvw2mY7nOke1wsq9pCkk91DGo3gpFbfuc5smqRA)” section, specify the items the graders have to look for when they review the candidates' answers.
* Finally, under "Additional Settings", check "This interview requires scorecards to be submitted" and leave unchecked "Hide candidate name and details from grader."
* Click "Save".
* The next stage is the `Screening` stage, which should be standard across the board. It is recommended to click "Edit" on this stage, scroll to the bottom, and choose the recruiter as the default interviewer.
* The next stage is the `Team Interview`, where the candidates will meet with peers and the hiring manager.
* Under this stage, there will be different types of interviews. They are typically called "Manager Interview", "Peer Interview 1", “Peer Interview 2" and "Executive Interview."
* For each interview in the stage, click "Edit" next to it. First, select the appropriate attributes to focus on in that interview. Then include the purpose of the call and sample questions the interviewer should ask.
* The two "Additional Settings" should both be checked.
* Then click "Save".
* The next stage is for `Reference` checks, with two sections for a former manager and a former peer of the candidate. These can be customized as needed.
* The last stage is the `Offer` stage. (Please refer creating a vacancy or offer approval flow)

**Assignment of hiring team**

* The next section is assigning the hiring team & the related access restrictions.
* The first step is to scroll down to the "Who Can See This Job" to assign permissions to the team members who will need access. Continue scrolling to "Job Admin: Hiring Manager" and click the pencil and add the Hiring Manager(s), including their Managers, Directors, and Executive, then click save.
* Scroll back to the top of the page and select the main people responsible for the job. Under "Hiring Managers", click the pencil and select the Hiring Manager for the vacancy and click save. Under "Recruiters", select the recruiter assigned to the particular division. Under "Coordinators", select the coordinators assigned to the  division.
* Once the hiring team is added, click "This looks good, NEXT" at the right.
* The next section is the Approvals section. (Please refer creating a vacancy or offer approval flow).
* Double check that the vacancy posted correctly in Greenhouse.

**Creating a vacancy or offer approval flow (If unavailable)**

* Log in to Greenhouse and go to the configure section by clicking the gear at the top right corner.
* Then click "Approvals", where there will be two columns (job approvals on the left and offer approvals on the right) separated out by divisions/departments. Each division/department that either has a unique division in Greenhouse OR a unique executive should have its own section.
* Scroll to the bottom of the page and click "Add Approval by office/department".
* Then choose what division/department. Do not select an office. Click "Create".
* Under both the job and offer approvals columns for the new section, if there are any automatic additions that populate, remove them by hovering over them and clicking the "x".
* Click "Add Approval Step" under the jobs approval column. Then click "Add" and search for the appropriate team member for Step 1 and select their name. If there should be more than one team member, click "Add" again and search for the team member’s name and select their name.<br>
  Note #1: The vacancy approval flow should follow the below mentioned approval flow format.
* Then, Click "Save".
* Perform the same process to update offer approval flows.<br>
  Note #2: The offer approval flow should follow the below mentioned approval flow.

    |Job approvals flow (Note #1)|
    |----|
    |Step 1: People Ops Analyst (only 1 required)|
    |Step 2: FP&A (only 1 required)|
    |Step 3: Executive of division/department (only 1 required if multiple people are listed)|
    |Step 4: FP&A (only 1 required) - 2nd stage approval or Official Job approval|

    |Job offers flow format (Note #2)|
    |----|
    |Step 1: People Ops Analyst (only 1 required)|
    |Step 2: FP&A (only 1 required)|
    |Step 3: Executive of division/department (only 1 required if multiple people are listed)|
    |Step 4: CPO and CEO (only 1 required) for Director-level roles and above|

**Changing an existing section of approvals**

* If the participants in an approval step are to be changed, click the pencil, then it can be removed and/or added team members.
* If an approval step is to be deleted, then delete an approval step in its entirety, click the "x".
* If the approval order is to be changed, then click and drag that step to the appropriate placing.

**Publicizing the vacancy**

* The careers page currently links to a general 'Talent Community' submission, which is not tied to any jobs.
    * However, a select few NQR jobs have been posted externally (i.e. they are accepting inbound applications).
* Team Members submit their referrals via an Issue, which mirrors the 'Referral In-Take Questionnaire.' Details about this process can be seen [here](/handbook/hiring/referral-process/)
* The hiring team may advertise the vacancy through the following sites and is open to posting to more, in order to widely publish a variety of vacancies.

    |Sl.no|Particulars|
    |---|----|
    |1|Alumni Post|
    |2|LinkedIn|
    |3|Remote Base|
    |4|WeWorkRemotely|
    |5|RemoteOK|
    |6|Indeed Prime|
    |7|Ruby Weekly|
    |8|Glassdoor|
    |9|AngelList|

* The hiring team can create specific tracking links through Greenhouse in order to include the specific source of different job boards.

**Creating tracking links in Greenhouse for external postings**
* Click the [Configure icon](https://docs.google.com/document/d/1Fxpdg3S_5q7J5JqTR7JdVJ0Cc6ijLP0vRK7ilh6pU7c/edit) in the upper-right hand corner and navigate to Job Board on the left-hand panel.
* Select a job board from the subsequent list and click the ellipsis inline with the job board. Click [Tracking Link](https://docs.google.com/document/d/1e8yL2M0Sc1_ixTbwpEamn_d70xXd26fykf4nwy1McB8/edit) from the dropdown menu.
* Use the [Get a Tracking Link dialog box](https://docs.google.com/document/d/14Jn5rNltJzTgVVKjR05UEmAyyhekVc4La_pRiwGxdIQ/edit) to configure the Who gets credit and Source fields. Click Create Link when finished.
    * A tracking link for the job board will be generated in the provided field. Copy the tracking link and share.

**Offer Packages**

After completion of the interview and candidate selection, approval in Greenhouse needs to be initiated.

**Offer Package Creation**

* The hiring manager will work with the recruiter on the offer details and the recruiter will be responsible for submitting the official offer details through Greenhouse.
* To create the offer package, move the candidate to the "Offer" stage in Greenhouse and select "Manage Offer." Input all required and relevant information, ensuring its correctness, and submit; then click Request Approval **(H2R.C.02)**.
* Information in the offer package for counter offers should include the following in the "Approval Notes" section:

    * New offer
    * Original offer
    * Candidate's salary expectation beginning of process
    * Candidate's counter offer<br><br>

* Anyone making comments regarding an offer should make sure to @mention the recruiter and hiring manager:

    1. The People Ops Analyst will receive an email notifying them of the offer.
    1. The People Ops Analyst will ensure the compensation is in line with compensation benchmarks.
    1. Only one approval is needed in order to move forward.
    1. If the hire is not in a low location factor area above 0.9, the e-group member responsible for the function and the CFO will be notified.
    1. The FP&A will receive a notification to approve.
    1. The executive of the division will then receive a notification to approve.
    1. Lastly, for Director-level roles and above, the CEO and CPO will receive a notification to approve.
    1. Only one approval is required in order to move forward with the offer.
    1. Typically, the CPO will provide the final approval, but if the CPO is out of office, the CEO will be the final approver.

* It is recommended to also ping approvers, especially the executive (and CEO if needed) in Slack with the message

**Template for Message**

"Hiring approval needed for [Candidate Name] for [Position]" with a link to the candidate profile.

To create the link, search for the candidate in Greenhouse, select the candidate, go to their offer details page, and copy the link. Do not copy a link from a different section of their candidate profile.

**Final offer approval**

For pending offer approvals needed from the CPO/CEO, there is an #offers Slack channel where the requests should be added.

Please ensure the ping has:

 1. Name
 1. Position
 1. For re-approvals clearly indicate what changed and why

If the role is for an individual contributor, the CPO or CEO do not need to approve. However, please @mention the CEO in the '#offers' channel with "Offer has been extended for [Candidate Name] for [Position]" and a link to the candidates Greenhouse profile.

**Communicating the Offer**

Once the offer package has been approved by the approval chain, the verbal offer will be given, which will be followed by an official contract, which is sent through Greenhouse.

Offers made to new team members should be documented in Greenhouse through the notes thread between the person authorized to make the offer and the Candidate Experience Specialist.

Greenhouse offer details should be updated as necessary to reflect any changes including start date. Sections updated that will trigger re-approval are noted in Greenhouse.

**Employment Contracts**

* One person from the recruiting team (typically the Candidate Experience Specialists) will prepare the contract. Following are the steps for preparation of employment contract:

    1. Check all aspects of the offer.
    1. Generate the contract within Greenhouse using a template based on the details found in the offer package.
    1. Contact the recruiter or new team member to gather any missing pieces of information (note: the address can be found on the background check information page).
    1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by the Senior Director of Legal Affairs or a People Operations Analyst.
    1. Stage the contract in DocuSign from within Greenhouse, which emails the contract to the signing parties, with the recruiter, recruiting manager, and the hiring manager cc'd. One of the Recruiting Managers will sign all contracts before they go to the candidate for signatures. If the Recruiting Managers are out of office, the VP of Recruiting will sign.
    1. Enter the new team member's details on the Google sheet GitLab Onboarding Tracker and continually update it during the offer process.
    1. When the contract is signed by all parties, the Candidate Experience Specialist verifies that the start date in Greenhouse is correct; then they will mark the candidate in Greenhouse as "Hired" and export to BambooHR (do not export to BambooHR if it is an internal hire).
    1. The Candidate Experience Specialist will upload the signed contract and the completed background check into the BambooHR profile.
    1. The Candidate Experience Specialist will email the new team member the Welcome Email from Greenhouse with a Cc to the recruiter, IT Ops and hiring manager.
    1. Instructions on the Notebook Ordering Process are included with this email.
    1. The recruiter will remove the vacancy in Greenhouse and dispose any remaining candidate profiles if necessary. Once complete, the recruiter will ping the Candidate Experience Specialist to close the role or close the role themselves.
    1. The final step before handing off to the People Operations Specialists is for the Candidate Experience Specialist to ensure all fields for the new team member on the GitLab Onboarding Tracker are updated and complete through the "GitLab Welcome Email Sent" column.

**Onboarding**

On completion of recruitment and release of offer, People Experience Associates use a GitLab internal automation script to create an Onboarding Issue.

Onboarding issues keeps track of all the tasks to be completed before onboarding a candidate. People Experience Associates assigns tasks to various team members in the [onboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md).

**Adding a New Team Member to BambooHR**

As part of onboarding, People Experience Associates will process new hires in BambooHR. Following are the activities performed by People Experience Associates in BambooHR to complete and update new hires' profiles.

* Effective Date - Hire Date
* Location - Which entity the new team member is contracted through.
* Division - Enter the appropriate division from the dropdown.
* Department - Enter the appropriate department from the dropdown.
* Job Title - Choose the job title. If it does not exist already, scroll to the bottom, click "Add New" and save the new job title.
* Reports To - Select their manager.
* Enter whether the team member is part-time or full-time. Any comments add them to the compensation table.
* Employment Status
* Enter the hire date and set the status to active. Also leave a comment if there is anything of note in the contract.
* Pay Rate - Entered as if it were a payroll amount. For example, a US employee would be entered as their yearly amount divided by 24 payrolls in a year. A contractor would have their monthly contract amount listed.
* Pay Per - Monthly for contractors and employees paid once per month, Pay Period for all other employees
* Pay Type - Use either Salary or Hourly for employees, or Contract for contractors.
* Pay Schedule - Select the pay period. Currently we have twice a month for the US, and monthly for all others.
* [Pay Frequency](/handbook/people-group/contracts-and-international-expansion/#team-member-types-by-country):
    * 12.96 For GitLab B.V. employees in the Netherlands
    * 13.92 for GitLab B.V. employees in Belgium
    * 24 for GitLab Inc. and GitLab Federal employees in the United States
    * 24 for GitLab Canada Corp in Canada
    * 12 for everyone else paid monthly

**Auditing Bamboo HR entries:**

All new profiles are usually audited on Day 1 or 2 of a new team member’s first week by the Total Rewards Analyst.  

The Total Rewards Analyst also updates a new team member’s BambooHR access level at this time, from Self-Service (which is the default level) to either Managers; Contractor Self-Service; or both if necessary.

New team member compensation information is also reviewed weekly when the Total Rewards Analysts review the Payroll Change Report. Once a month an audit is conducted from all payroll providers to ensure the salary information matches BambooHR.

## 2. Employee Master Creation and updates

[Link to flowchart](https://drive.google.com/open?id=1TP-cxXdfDhZ0bhZ3Gvb1qWrl6exuU2U5V08oVLOyU_0)

**Process for US Team Members**

GitLab uses ADP for payroll processing for all US-based team members only. Employee masters are created in ADP by the Payroll team before payroll processing based on inputs from BambooHR. **(H2R.C.10, H2R.C.11, H2R.C.12, H2R.C.14 and H2R.C.15)**.

**Creation of Team Member Profiles in ADP**

Employee profiles are created in ADP. The following steps are followed to create a team member's profile in ADP **(H2R.C.04, H2R.C.05, H2R.C.06, and H2R.C.07)**

* Once a new team member's I-9 form is completed and their employment is verified by the People Experience Associate, according to the onboarding issue task, the People Experience Associate must tag the Total Rewards Analyst.
* The Total Rewards Analyst audits the new team member's BambooHR profile and I-9 certification documents, and then checks the ["I-9 Processed" checkbox](https://docs.google.com/document/d/1uhkqq7a6dtcsJrAek9lHZxxhOjRJZCTXRpH5okLMqkQ/edit?usp=sharing), located on the Personal tab in the team member's BambooHR profile.
* The Total Rewards Analyst then comments in the team member's onboarding issue tagging the Payroll Specialists and stating `Ready to add to ADP`, according to the onboarding issue task.
* An [automated batch job](https://docs.google.com/document/d/1XwzVrI-GEd87JpNZyIdsfcWwZmP5C6IVgy9wj1ro4Qs/edit?usp=sharing) that runs daily between 7:00 AM and 7:15 AM (CST) by modulus picks up any new I-9 verified team member profiles from BambooHR and loads them into ADP.
* BambooHR is integrated with ADP and team member profiles are created in ADP automatically.
* The Payroll Specialists review the newly created team member profiles in ADP and compare them to the BambooHR profiles for any errors. 
* The Payroll Specialists [email each team member](https://docs.google.com/document/d/1KVCc3ZDqlt7yaGz4d7J0gEIyfgX9omazP0C60U18dWE/edit) with information about completing their ADP registration and accessing their accounts.
* Once done, the Payroll Specialists check off the Payroll [task](https://docs.google.com/document/d/1dM8cZZMYI88s_6T-SfawFX9mRDWGIVZIN8kjlVewjFY/edit)) stating that the new team member has been added to ADP and that an invitation to ADP has been sent to them in the relevant team member's onboarding issue.
* The Payroll Specialists then comment in the team member's onboarding issue notifying the Total Rewards Analysts that the team members have been successfully added to ADP.
* The Payroll Specialists also run the [New Hire report in BambooHR](https://docs.google.com/document/d/14Vsxkw9kkVSxasiisFoUCp7RI5z4mn5ZxTqFZNp6A44/edit) at the start of every week for list of new team members by start date, to ensure that they are aware of all new team members, in the case that they are not tagged in an onboarding issue. 

**Team Member Terminations in ADP**

Team Member Terminations are updated on ADP in the following way:

* When a team member's offboarding issue is created, the relevant Payroll Specialists are tagged.
* In [ADP](https://docs.google.com/document/d/1sha3hvrHj85IFYPvFEDckD1udfbTwr2OMVkHnrTLzvc/edit), the Payroll Specialists navigate to People > Employment> Employment profile.
* The Payroll Specialists [search for the Team Member](https://docs.google.com/document/d/1WzXw7QIxKJIPgOOS_pweVYCqJiKCRUfoi26G4k0NHkA/edit).
* The Payroll Specialists update the [employee status](https://docs.google.com/document/d/18JCaxTOvABEP7uiBrfxTswqQ-aqltmwmJloRYDGFgDw/edit) from Active to either Leave, Terminated or Deceased.
* The Payroll Specialists start the [Termination Wizard](https://docs.google.com/document/d/1ScXsbvREHsCBdSCKjwG9F9999TLzv0ju1zx-Lr6sWNw/edit).
* The Payroll Specialists enter [termination date](https://docs.google.com/document/d/11j1nXkcUDL7uwHOobUA0fc3-bjAEbqqIQJR_DCHgDgw/edit), termination reason (according to the information in BambooHR) and choose voluntary or involuntary. If voluntary, keep eligible for rehire checked. Check if Severance is paid if applicable.
* The Payroll Specialists then make sure that Autopay is canceled. They do not cancel Direct Deposit, in case of a future commission payment, or another type of payment, and update in [ADP](https://docs.google.com/document/d/1wXHTjNW21gUG3Hh0bzOYIwTTzpR51nhPEQgE1WrFneI/edit).
* The Payroll Specialists comment in the [offboarding issue](https://docs.google.com/document/d/1ABhpmORr88gVUDHArxrGMLpsmh7c_38cnwgp66yykPg/edit) once all tasks are completed in ADP.

On every paycheck date, an audit of all entity payroll providers is conducted to ensure the salary information matches BambooHR. This is done by Payroll Specialists and approved by the Senior Director, Corporate Controller.

**Payroll-related Data Process for New Team Members in Non-US Entities**

For [all countries where GitLab has an entity](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#gitlab-entities) the Payroll Specialist will extract the payroll data and send to the payroll provider, who will in turn add the new employees for the respective countries.

**Payroll-related Data Process for New Team Members employed or contracted through PEOs**

For PEOs, the new employee information to be added is sent by the recruitment team directly to the PEOs.

## 3. Payroll Processing for US

[Link to flowchart](https://drive.google.com/open?id=1WqGy-0YpYK405NbxSW1fzuVX63cZVwAUfvOwIq-JsEo)

This section explains about processing payroll for US employees. The process starts with opening payroll cycle for the check date  and updating payroll processing files per changes from BambooHR. Payroll team reviews payroll benefits, one-time changes such as bonus and commissions based on inputs received from various teams and accepts payroll period in ADP (This ends the payroll cycle).

**Opening Payroll**

* In [ADP](https://docs.google.com/document/d/1I-caVIWiBEln35z0HSPLnTXe9XC-ee-OdCEri-zZkkw/edit), navigate to Process> Payroll> Payroll cycle and continue.
* Select [start new cycle](https://docs.google.com/document/d/1w5FaJO9qfBfJDzqvnQIkq0rlSVJJB2odabsRFn2Y4C4/edit).
* Then click [continue](https://docs.google.com/document/d/1LN4wT7JJtG_z1gaLp1tmuCfxJA6OO0wHVONaJ9VdjRM/edit).

**Payroll Changes**

* A [Google sheet](https://docs.google.com/document/d/1Miljaad0sgibrublHr2Q3DBHGigndZa6mp0Zw9KzNf4/edit) is  shared from PeopleOps for payroll changes every paycheck date (varies based on [location](ur/handbook/finance/payroll/#pay-datel)).
* Information is entered into ADP using this Google Sheet by the Payroll Specialist, and reviewed by the Senior Manager, Global  Payroll and Payments.
* Entering [promotion details(salary and title change)](https://docs.google.com/document/d/1Rd6BEYDcRwGGcXlxcsTyFNmBmfQTSuzdTFuY6Ted6S4/edit).

**Salary Changes**

* Review the [existing salary](https://docs.google.com/document/d/1WoPQUX2m1-2etgB5FQiqi-VZZHcYiRMdnIUpkrzsZ_o/edit) for the employee.
* Update the [date of effective change, reason and rate change calculator](https://docs.google.com/document/d/1gQZFSToS1CP0zAgw-nWHc_pmwpGfx98MJeEC7mMAsZs/edit)

**Designation Changes**

[Edit the effective date, Job Title, Position start date, and NAICS(North American Industry Classification System) workers comp and then click done](https://docs.google.com/document/d/1a1Q6ufhL7cw_RXiil6jb7E5JDkUNFQEfvfIw4OfZcM4/edit).

**Entering Bonus**

* In [ADP](https://docs.google.com/document/d/1pdNE0Z6Q_aLn7eiymIqBEAdHO6yOiV9sHA_mx2dCKo8/edit) click on Process > Payroll> Paydata.
* Select [“ADD A BATCH”](https://docs.google.com/document/d/1WSSFU8Rte3MAE91GAvMgj2eaaoYkvhPHsH9TSRN1Auw/edit).
* Update [Batch Number, description and select “Paydata template” as Bonus](https://docs.google.com/document/d/113jCr7FsnXDlVYLDHmfxW-jKWSFozFgSL1GK2VPZzeY/edit). After updating proceed to click “next”.
* [Select the employees](https://docs.google.com/document/d/1FX9wOQI1Uf-Dz7y9PtFpdscQWyaJt1-7mgTEvHSfu9o/edit) for whom the bonus is to be processed.
* [Enter in the amount](https://docs.google.com/document/d/1rSn-BcODnn38oI0fDnskHnNwLvp4TvIcSCZKTvPz-FY/edit) and then proceed to click “Done”.

**Payroll Processing for Hourly team members**

* Navigate to [google timesheet](https://docs.google.com/document/d/1I4ntM9VMgKniFlFBfSoL20eQssToieZ15zwpJfet_hU/edit) and enter hours in pay data batch in ADP.
* [Add a Paydata batch](https://docs.google.com/document/d/1iN8ezl9ne6f2a1E486yfd_Bw8oECR8lit4r6ofqys1Q/edit) and enter description as hourly and use the Paydata template as “ADP Basic paydata”.
* Select the employees for whom salary is to be processed.
* [Enter in the amount](https://docs.google.com/document/d/1rcbeoGupWyjloAuzEDwADBiHL-w5a5PxeuVoFrm1_lw/edit) and then proceed to click “Done”.

**Processing Payroll Benefits**

* Benefits are processed by Lumity in the US.
* For specific information on benefits, please [click here](/handbook/total-rewards/benefits/)
* [Email](https://docs.google.com/document/d/167v2pRyP9gO-CFOW5JYFqpnL74nptgWsP9a5uPnFVpQ/edit) received from Lumity(Health insurance service provider) in a password protected benefits file.
* Update any employee benefits information from the [file](https://docs.google.com/document/d/185HglkNFHClfccfwOO7vRZFuJYAC4xQRnT68bh8pBi8/edit) to ADP.
* Payroll prepares the data into an [import template](https://docs.google.com/document/d/1V9K7mOFg9UCtvGbLkaPScbYBnNc5XtoXKioEIbrDTec/edit) for mass changes on ADP.

**Processing Bonus**

* All bonus components are to be approved by the CFO before the payment is made. The CFO approves the bonus by inserting a comment on the [Bonus calculation](https://docs.google.com/document/d/1c4v4rkzXy-qamREcdZAYaSBHMtpyLwtE2f3vV5VXHpA/edit) spreadsheet tagging the payroll team. When a comment is inserted in google spreadsheets, an automated email is sent by Google to all the users who have access to spreadsheets and to users tagged in the comment. The Payroll Specialist will print out a PDF copy of the email containing the CFO’s approval as a supporting document and will update the bonus components in ADP.
* Navigate to [bonus sheets](https://docs.google.com/document/d/1RzorxVAo7FzHntYZoFq-4xe6bywHHJQF8WBy8ChwdpM/edit#heading=h.a0t6xd56yisz) and verify if updates are as per approval comment. Payroll will follow up for approvals from the Managers (who are giving the bonus to the employee) and CFO.
* Update the SDR payouts as per the [ADP upload format](https://docs.google.com/document/d/1xKvQDiAD4k0DgnGSqn-Vdsi742nEGoOyEhHXDtaGRy8/edit).
* Use import template to enter information, [import](https://docs.google.com/document/d/1xEhj3Z2IcZD09oxea_BNgIH2zM8-NwzuwTx0u2J93Pc/edit) the data in ADP.
* [Select the file and upload](https://docs.google.com/document/d/1FA__ABAdf31OOpVZf4S0YueXLb9bOeqzzWWHbRQLg5o/edit) the file for processing payroll.

**Processing Commissions**

* The Commission Manager prepares the working and sends it to PAO for approval by tagging the PAO in the spreadsheet. The PAO approves the commission calculation in the spreadsheet tagging the payroll team. When a comment is inserted in google spreadsheets, an automated email is sent by Google to all the users who have access to spreadsheets and to users tagged in the comment.  The Payroll Specialist will print out a PDF copy of the email containing the PAO’s approval as a supporting document and will update the commission amount in ADP.
* Navigate to [commissions google sheet](https://docs.google.com/document/d/1-yGDrHpC7FKQPSQwo0yN3BBpUiKKatuBNJIqMd6MSn4/edit), enter information in pay data batch in ADP just like bonus example.

**Payroll Closing procedures**

* Go to [payroll cycle](https://docs.google.com/document/d/1VmgGPfD8hcWeBMHUpyYhMJ3ivd7fSdFGsSxipof7U-Q/edit) in ADP and click on changes and click on view all.
* Below [reports are downloaded](https://docs.google.com/document/d/1VmgGPfD8hcWeBMHUpyYhMJ3ivd7fSdFGsSxipof7U-Q/edit) and checked before closing:
  * Automatic pay cancelled  - shows any employees not receiving salary hours
  * Active hourly/daily employees without hours & earnings - shows employees without pay
  * Employee changes-shows any changes to an employee’s record
  * Inactive employees with paydata - shows term employees with pay
  * Pay data summary - shows all batches that were imported/entered in pay data
* In the [payroll cycle screen](https://docs.google.com/document/d/1Nq5ObWOipcRuQ_Sfo3_MPm-fDLVDRcZEpSx_OfIQzE4/edit), click on “Preview payroll” - after checking the changes on the payroll register.
* [Preview the payroll](https://docs.google.com/document/d/1IhluYoZi4ZOxe_WnO95j2r93aDhge2Sskr4YiZ2a80I/edit) and then click continue.
* The [preview payroll is generated](https://docs.google.com/document/d/1WL3e9cYfo-jrTv8fgDwuAynXsImptzU8hC436Eqzp18/edit) and reviewed by the Payroll Specialist. Final approval is given by the Senior Manager, Global Payroll and Payments, either by signing on the payroll computations or via email.
* Department and tax withholding  validation:
  * The two Payroll Specialists will validate the following  for each  employee on every paycheck date:
  * Department and cost center per BambooHR compared with the cost center and department details for each employee on ADP.  This process is completed using the department report downloaded from bambooHR.
  * The tax withholding is verified for each employee. The tax withholding for every US employee is based on the "live in'' state and "Work in" state available on BambooHR. The details on BambooHR are compared with ADP through an ADP report with the "Live in" and "Work in" fields.
  * The reconciliation for department and tax withholding is performed for accuracy of the payroll data as the final step
  * The two Payroll specialists performing the reconciliation will review each other's work to ensure that right information is entered in ADP from the payroll changes noted.
  * The Sr. Manager, Global Payroll and Payments  will provide final sign off by performing spot checks on employee data.
* [Accept payroll](https://docs.google.com/document/d/11Rs5QxX94aLmLhXPIl1d46X7ENclYzw0mUm4WQjLW-I/edit).

**Download these reports to send to accounting and Lumity (benefits provider)**

* Deductions by check date - match deduction totals including HSA deductions with payroll register totals. Send a password-protected file to Lumity. Save in the Lumity google folder.
* Commission and bonus by check date - match totals with payroll register totals report. Save in accounting google folder.
* Misc. deduction report - explain what deduction is for if any entered for that check date. Save in accounting google folder.
* Payroll Register - save in payroll google folder.
* Payroll Register totals - save a copy in accounting and Lumity google folder.
* Payroll Summary - save a copy in accounting google folder.
* Statistical summary - save a copy in the accounting google folder.
* GL report for accounting - summary of payroll allocated per department with the correct GL account to book entries on NetSuite.

**Accounting for US Payroll**

* The accounting team receives a file from Payroll every paycheck cycle called the GL Mapping File. This [file](https://docs.google.com/document/d/1-QIXlmepCS3tFm1QFUQjZ5koKRSeIpQDsndoSM_nSXA/edit?usp=sharing) comes from ADP.
* The [Net Pay and Taxes and Garnishments](https://docs.google.com/document/d/1FZt-f442njeGWt-b6LW30hQd35UqvdrzDHxVgC9FtkI/edit?usp=sharing) are automatically withdrawn from Gitlab’s account. These amounts are tied out, and the accounting team checks the amounts debited, and works with Payroll for any differences in amounts.
* The amounts are also tied back to the [Stat report](https://docs.google.com/document/d/1cWHSasaMYuGLs8vGBHvyfOW4K-_vMWEUt1MgS6lbB0o/edit?usp=sharing) from ADP. So there ends up being a 3-way tie-up between the bank accounts, the GL Mapping file, and the Stat report.
* Payroll Taxes, Salaries, Benefits, bonuses, and 401k match (employer-side) deductions are all mapped on this [file](https://docs.google.com/document/d/1KPMOVuG0PdDSAhBHABMoY_u9-3XDh1f-PSJkSZjhRiQ/edit?usp=sharing) at a department level.
* The account mapping was set up by Payroll and Accounting together previously on ADP and is followed to date. The responsibility of appropriate mapping lies with the Payroll Team.
* This file is verified and then imported onto NetSuite.

## 4. Payroll processing for Non-US

This section explains about payroll processing for PEOs and contract employees (Non-US).

**Payroll processing for NON-US entities, PEOs, and Contract employees** **(H2R.C.10, H2R.C.11, H2R.C.12, and H2R.C.13)**
* The [payroll change report(s) Google Sheets](https://docs.google.com/document/d/1vK8KfN4EyDDk0KlzgECX3r-8kp_eDRv9oVO6RBYCB_0/edit) for all Non-US locations are received from People Ops per paycheck date (varies based on [location](ur/handbook/finance/payroll/#pay-datel)).
* Enter in the [password for the sheet](https://docs.google.com/document/d/1hekX6aprj4tYxSXjdbefTyn9b6Lb8D-LKAKItpK2Xbs/edit)
* Verify if any [changes](https://docs.google.com/document/d/1-qT_jTMcujUzex7LSNAgHY24ec3h6805fTff2q3Zi5c/edit) have occurred in the location.
* The [commission process](https://docs.google.com/document/d/1GdNsBIR0h2SDUsp-AKEcV4obVEF2ebgTfhU3X3wAt4g/edit) is the same as the US Payroll. Please “Processing Commissions” in the US Payroll.
* The [SDR bonus payments](https://docs.google.com/document/d/1bm1gEuexdT-FFZwWH5UU22hSo_k5hPO5L9jOFDAIVTY/edit) come from Sales and are approved by the CFO. Refer to the "Processing Bonus" under US Payroll for more information.
* All this info is sent to the respective locations’ Payroll Service Providers by the Payroll Specialist EMEA.
* Local payroll providers will process this information, and send the payroll report to the Jr. Payroll Specialist for review and approval.
* The Jr. Payroll Specialist will review and notify the Payroll Specialist EMEA for final review and approval. This approval is given via [email](https://docs.google.com/document/d/11r648xzjpu8gODVECMiOQ2yQ29v2gfA0Wr9RFDFe2Ts/edit) **(H2R.C.11)**.
* Once that is completed then the Payroll Specialist EMEA sends the approval email to the payroll provider.<br><br>

**For payroll remittance of Non-US entities**

  **Belgium**<br>
  Once approved, the Payroll Specialist EMEA will send a funding request to the AP team for remittance of salary **(H2R.C.14 and H2R.C.15)**.

  **Netherlands & Germany**<br>
  * Once approved, the Payroll provider in Netherland and Germany will release the payment to the employee, and an invoice is sent to AP by the Payroll provider for release the payment **(H2R.C.14 and H2R.C.15)**.
  * In the case of Germany the tax due for employee tax is debited in the bank account by the government agencies directly and not remitted by the payroll provider.

  **UK**<br>
  * Once approved, the payment is auto-debited from GitLab’s bank account **(H2R.C.14 and H2R.C.15)**.
  * One week before the payroll check date an email is sent by the Payroll team to the Controller and the payroll provider (Vistra) to confirm if there is sufficient balance in the bank account.
  * The check is done to ensure that there is sufficient balance one day before the paycheck day.

  **Australia**<br>
  Once approved, the Director - Corporate Controller will send a funding request to the AP team, and AP  will wire the payroll funding to IIPAY. **(H2R.C.14 and H2R.C.15)**. For more info [click here](ur/handbook/finance/payroll/#peol).

**For payroll remittance of PEOs**

* PEOs are paid through AP. The payroll change reports are shared with PEO, and the PEO will send out invoices after processing them. The invoices are sent to the Payroll Team, and after verification of the invoices, they are passed on to the AP team. **(H2R.C.14 and H2R.C.15)**. (For more info [click here.](/handbook/finance/payroll/#peo).
* Verify the [invoice](https://docs.google.com/document/d/1RJRMHx4rLQ4qWNak7NGHdYNA4RusVgOFn7YtGfHS5CU/edit) sent by the PEO.
   * The PEOs will raise an invoice with the total amount and in some cases, the PEO will attach a detailed breakup per employee. The following are the PEOs that GitLab deals with and the types of invoices provided by them:
     * Safeguard - a high-level invoice with an attachment containing employee wise breakup is provided
     * CXC - a detailed invoice with employee wise break up is provided
     * Global Upside (starting May 2020) - to be provided later.
* Login to [Hello Sign](https://docs.google.com/document/d/16UqMMyBeg1Z7fhgc51Uzob8N_bpfmek-wxM6onZSj0w/edit).
* Sign off the [invoice](https://docs.google.com/document/d/1iOWnvNeQI7w2IWqOaTy2qKC_qmh6K2NLxRHEqCngJ2Q/edit) and send it for payment processing.

**For payroll remittance of contractors (H2R.C.09)**

* GitLab has contractors in New Zealand and South Africa that are not attached to either a PEO or C2C organisation that are paid by AP directly based on invoices submitted by the contractors. Contractors attached to an organization (iiPay, CXC Global) are paid through  AP.
* Contractors linked with iiPay, will raise their payroll invoices monthly to iiPay. iiPay in turn will collate all the invoices received from the contractors and submits a single invoice to GitLab on  Bamboo HR. iiPay is paid directly through the AP route to the bank account provided by iiPay on BambooHR. For more info [click here](/handbook/finance/payroll/#contractors).
* [Email reminders](https://docs.google.com/document/d/1s_G07CNbLR3XBoxyuOYud5Qr5u02PK8gba2N0Efo_X0/edit) are sent by Jr. Payroll Specialist to add salary invoice to Bamboo HR and expenses to Expensify **(H2R.C.13)**.
* iiPay will upload the monthly invoice in [Bamboo HR](https://docs.google.com/document/d/1hmJnRpKZdAAjJBs5bW6XujjIeZsAB9R6UFdu_IgXj4Y/edit) for contractors link to it.
* Expenses for contractors linked with iiPay are uploaded onto [Expensify](https://docs.google.com/document/d/1jW1QJtpcMQGhpEq31XLD8PvVQkA5hlJRUMsiVCyrQ1Q/edit) which is reviewed by the reporting managers and the expense specialist
* The contract employee sends the [invoice](https://docs.google.com/document/d/1e7Tc4vYQ0wG-S6IEo1DPB2iGU-2eCiBObokqyaQCbLM/edit) for payment to the Non-US Payroll inbox which is reviewed by the Payroll Specialist.
* After the Payroll Specialist approves the [invoice](https://docs.google.com/document/d/1cGFnexOFMTSMR7TjZqjlMGpHUZ2iWGRjnIG1h4ZZRR0/edit).
* Invoice sent by the contractor is signed by Payroll Specialist using [Hellosign ](https://docs.google.com/document/d/14eblbiWRougAyhne0_m8sBYENWW_oEBr-3-3ppaUUFs/edit)and is sent for payment processing to AP **(H2R.C.14 and H2R.C.15)**.

After payroll takes place, the payroll reports and invoices are processed by AP.

**Accounting of Non - US entities Payroll**

**BV (Netherlands,Belgium)**

**Netherlands**

* The Journal Entries department list (report) is downloaded from HRSaavy by the Payroll team. This list shows the amount under each account department wise. This report is a pdf. This report is downloaded and sent to the accounting team.
* The account numbers are from HRSaavy, and must be matched with the ones in NetSuite. This is done by the accounting team. Since the account names were in Dutch when they first came out of HRSaavy, they needed to be manually translated and matched with the accounts in Netsuite before the amounts could be entered. The Accounting team has now worked with the payroll provider so that their JE reports say Gitlab’s account names. The GL account mapping is provided based on the information provided by the payroll provider.
* They are accounted for by the Senior Accounting Manager, and reviewed by the Senior Accounting Operations Manager.
* The amounts are manually entered onto an excel file template, and then uploaded to NetSuite.

**Belgium**

- The payroll report is received in the form of a password protected excel file from the payroll service provider.
- The given data needs to be mapped department wise. This is done manually by the accounting team by looking up the team members on Bamboo HR and entering in the departments manually.
- This file is verified and then imported onto NetSuite.
- The amount is tied to cash.

BV Contractors

- Gitlab contractors are paid by iiPay. Invoices are submitted to payroll, and payroll then compiles a list of all the contractors and their details. iiPay provides a reconciliation for the month (downloaded from their site) by Payroll and sent to the Accounting Team with how much was paid out and what currency was used for all the contractors. In the end, they summarize the payout and arrive at any difference (if any). This summary includes penny tests.
- The payroll team also provides a payroll instructions file, which shows the payment frequency, the bonus, the commissions, and the expenses of each contractor. Since the iiPay file doesn’t give a breakdown like this, the accounting team reconciles both files so that it can book entries on NetSuite. The accounts team summarizes this file so that it knows how much to book for each entry.
- The payroll team identifies the accounts where accruals are to be made (cases where the employee has started after the payroll run, issues with the employee’s bank account, etc) so that the accounting team can make them in NetSuite. These accruals are made as a part of the total Journal Entry for that month. These aren’t allowed to be reversed. The next month, the Senior Accounting Manager checks if those accounts were paid, and then reverses them as part of that month’s journal entry (manual reversal).
- iiPay payments are prepaid; they don’t auto-debit from GitLab’s bank account. The accounting team then reconciles the iiPay amount and the payroll  amount, identifies any differences, verifies the previous prepaid balance with iiPay, and comes up with what should be in the balance after this month’s payroll, and how much iiPay should be additionally funded
- GL mapping is done by the accounting team. Contractors have no taxes or benefits.

**Germany (GMBH)**

- The payroll report is received in the form of an excel file from the payroll service provider.
- The employees are summarized, department wise via a pivot table.
- This file is verified and then imported onto NetSuite.

**UK**

- The payroll report is received in the form of an excel file from the payroll service provider. Payroll provides the accounting team with this file.
- The given data is summarized and mapped, department wise via pivot table.
- The UK expense reimbursements are booked and paid through AP. The accounting team doesn’t book these expenses. (Net pay - expenses)
- This file is verified, and then imported onto NetSuite.
- The given data needs to be mapped department wise. This is done manually by the Sr. Manager Payroll by looking up the team members on Bamboo HR.

**Australia (PTY)**

- The payroll report is received in the form of an excel file from the payroll service provider. The Payroll team provides the accounting team with this file.
- The accounting team needs to calculate the estimated employer’s tax piece. This is because iiPay doesn’t process the employer’s tax liability. The calculated tax is paid by Payroll. This is passed as an accrual at 5%
- The employee tax is paid for the current month and the employer tax is paid in the next month.
- The total net pay and the taxes only include the employee tax portion, and not the employer’s tax.
- This file is verified and then imported onto NetSuite.

**PEOs (SafeGuard, CXC, Global Upside)**

These PEOs are paid through invoices and so go through AP. The AP team books these amounts on NetSuite. Tipalti automatically synchronizes with NetSuite (refer AP DTP).


## 5. Leave Management for payroll processing (LOA, leave of absence)

This section explains about updating leave of absence records for payroll processing

Recording Leave of Absence:

* Any time off is taken via PTO by Roots. PTO by Roots allows employees and managers to coordinate time off seamlessly using Slack. The integration from Slack and BambooHR automatically monitors PTO balances. PTO by Roots will automatically add any time off taken for sick and vacation accruals to BambooHR.<br>
The  Payroll Specialist is informed about any LOAs via the [payroll change report.](https://docs.google.com/document/d/10ABh_DchAhw49AjrZmtwMqIE5SJ8M63uzN8KodIOqRg/edit) **(H2R.C.08)**
* Updating an LOA in [ADP](https://docs.google.com/document/d/165rj1ESL_RDmECGAw4oLJ4xUQ71aBgruosuQDEVoGTw/edit), review and complete. Go to status and change the status to leave.
* In the next [screen](https://docs.google.com/document/d/15ObUjht7S_qVRFUJ2RkYBNKKMxgA_fVENB3AFma6fr0/edit), update the period of leave, the reason for leave and whether it is a paid leave or a non-paid leave.
* In the [Direct reports tab](https://docs.google.com/document/d/1heQmevsPacHuSU_fuOagp2rYh2oayRpns74AVpSQ7ZA/edit), assign any direct reports of the absentee to other managers (if applicable).
* In the [direct deposits tab](https://docs.google.com/document/d/1WrMowzZ4RGndYxGKIFmRN_GTYZvw1qS0yMGNLMeeFhg/edit), check the employee bank account number and name.
* In the [final payment tab](https://docs.google.com/document/d/1JjiU-ld3jn_4fS4pjiZLONAM04PN71nqrhIINoa-MuI/edit), select Yes
* In the [last Tab](https://docs.google.com/document/d/1j84BRHn_5e6sfnhxrepSD03FeFKAfjHMsVpSaLsXLIw/edit), Confirm the leave.

**Leave Accruals**

Leave Accruals take place for the employees of Non-US legal entities:

| Entity | Amount of Leave Accrued | Rollover | Encashment |
| ------ | ------ | ------ | ------ |
|GitLab UK|20 vacation days per year|yes, for the next 2 years|At the time of exit|
|GitLab BV|20 vacation days per year|upto June of the following year|At the time of exit|
|GitLab GmBH|20 vacation days per year|upto March 31st of the following year|At the time of exit|
|GitLab Belgium|20 vacation days per year|None|At the time of exit|
|GitLab PTY|20 vacation days per year|yes, if excessive can be requested to take by the employer|At the time of exit
|GitLab Canada Corp (All of Canada except SK, QC)|2 weeks after 1 year / 3 weeks after 5 years|None|At the end of the year|
|GitLab Canada Corp (SK, QC Only)|3 weeks after 1 year and going forward|None|At the end of the year|

**Notes:**

* The UK has announced relaxed measures on the carryover of annual leave due to COVID-19, so GitLab team members now have the opportunity to carry over their leave for the next 2 years.
* In Belgium annual leave accrual is based on what an employee works in the year preceding (can be at GitLab or elsewhere). If a full year is worked then the employee has full entitlement - 20 days, if not the entitlement is 0 days, and should the employee wish to take leave, the amount will be deducted from their 13th-month payment.
* In Australia, rollover is allowed at the end of the calendar year, and if the leave may be requested to be used up by the employer.
* In Canada, the amount of leave accrued varies depending on the location of the employee. In Saskatchewan and Quebec only, employees are entitled to 3 weeks of leave per year, provided they’ve been with GitLab for over a year.
* There is no leave rollover in Canada.  As per regulations, Canada essentially mandates that team members take their vacation.That is why there shouldn't be rollover and technically though you can pay unused vacation on the last check date of the year there may be penalties for doing so. Therefore it is incumbent on the team member's direct manager to ensure that the team member takes the time off and tracks it as vacation.
* For PEOs GitLab provides the PEO provider with the PTO taken dates and hours and each vendor will manage the PTO payout according to regulation for that country.


Leave Accruals are accounted for using reports from Bamboo HR. These reports are pulled by the Director, Corporate Controller.

* To pull the reports, log in to [Bamboo HR](https://docs.google.com/document/d/1epOmeNwJykKaAZYmQ7HQByzImRG6llbBgGSjm4IndLc/edit).
* Pull up the [Time off Balances report](https://docs.google.com/document/d/1MZiuGtVyMFF6dTbBz66_P5QenNtzmyjmTZYdvW1U7iM/edit). This report shows the Time off Balances for all employees.
* Set the time period needed, and click Apply. The [report](https://docs.google.com/document/d/1sTXIyxm0uAfI6M7fiNqYqDfYvEe0SI_w5A27usJ-7o0/edit) shows the employee name, beginning balance, number of accrued days, used days, scheduled days, adjustments, and the final ending balance. The ending balance is used for accounting purposes.
* A [second report](https://docs.google.com/document/d/1kQoe4yD8g6ALLv_ahahhlRFVFw4r0sDbl6dpBPj7KvQ/edit), Employee PTO Current Balance Accruals with Department, Entity, and rate, is also run.
* This [report](https://docs.google.com/document/d/1P1snUohu0OdgcgXKYDMYVOIRcNlASincJz5ljKBlwaE/edit) shows the employee’s department, entity, and salary information.
* These 2 reports are saved on [Google Drive](https://docs.google.com/document/d/1axMthzSlECKJQIQG7T8wdHvkVBZHCSu-ep_HMY6xyKk/edit) and consolidated.
* The final report has the employee’s department, entity, salary information, and ending balances. The liability is a factor of the employee’s hourly salary and the employee’s ending balance. The reports are consolidated and the final entries are booked by the General Ledger Accountant, and reviewed by the Senior Accounting Manager. Leave accruals are debited to 5029/6029 - Holiday allowance account and credited to 2226  Liability account.


## 6. Leave Management

[Link to flowchart](https://drive.google.com/open?id=1BHGj8bOBm1XJHIrRwdm5bOFmwNLQ8fbJKu-aB3PlSxg)

This section describes the process for termination of employees for payroll processing.

* Once an employee terminates and [off-boarding issues](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/offboarding.md) are raised by the Peoples Operations team.
* This will be used to track offboarding tasks before an employee leaves. The payroll team also receives an email and slack notification for termination from the People Operations team.
* [Email notification](https://docs.google.com/document/d/1r0CGr2X3F8T8OjX4F-k8Cpeik-KkpoFw1QLHFDB4k_8/edit) from PeopleOps
* In ADP, Navigate to  [people > Employment> Employment profile](https://docs.google.com/document/d/1WsHvwiu73qX9xo-lBs2B0C4D6TYEv-ZVXSUXRDeo6sM/edit).
* [Search](https://docs.google.com/document/d/1HSSiUcKi5JkXNawSAcxj4WJJWPzAwWOYh1E6_3mVqSs/edit) Team Member.
* Update the [employee status](https://docs.google.com/document/d/1CpipqZhVOu0T5RoA5mZbb6sF5c_t23i4kbRedDwhg3I/edit).
* Start [Termination Wizard](https://docs.google.com/document/d/1IiDXhIBu1Nnr3-qrLBCKOAvYxm6d3Bsa20pLuIaI7eg/edit).
* [Enter term date](https://docs.google.com/document/d/1dYA7ecjG42NYflGUhngJu6olpFTgpVMkACp12LrNMhI/edit), term reason (confirm with HR) and choose voluntary or involuntary. If voluntary keep eligible for rehire checked.
* Next, make sure [autopay](https://docs.google.com/document/d/1T-G8BXPv7hjKNScIRN8B83DT_69_G5qEg9XEV5sBeZ8/edit) is canceled, do not cancel direct deposit in case of a future commission payment. Then complete in ADP.
* Comment on the [off-boarding](https://docs.google.com/document/d/1rEIGVB2CVF6Ne2QyVIz4XhKmBWNWqf9BUImz1zrDNMQ/edit) issue once complete in ADP. **(H2R.C.16)**.
* Any final settlements are received via the payroll change report, and go through payroll processing.
* HR business Partner informs the Payroll team through a confidential slack channel about any termination
- based on the confirmation of the employee has signed a severance agreement from the HR business Partner,  the Payroll team will process the final settlement outside of the payroll processing timelines and not through the payroll change report
 **(H2R.C.16)**.
* More information on the employee exit process can be found here:
  * [Handbook link 1](/handbook/people-group/offboarding/)
  * [Handbook link 2](/handbook/people-group/offboarding/offboarding_guidelines/#stq=offboarding)
  * To see an example Offboarding issue, please [click here](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/offboarding.md)



