---
layout: handbook-page-toc
title: "Dev Section UX Weekly Update"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# 2020-05-01

| Milestone | GitLab Version |  Dates |
|-----------|----------------|--------|
|  Previous | [12.10](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/)  | [UX Retrospective](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/22)     |
|  Current  | [13.0](https://gitlab.com/groups/gitlab-org/-/milestones/42)                  | 2020-04-18 - 2020-05-17  |
|  Upcoming | [13.1](https://gitlab.com/groups/gitlab-org/-/milestones/47)                  | 2020-05-18 - 2020-06-17   |

## What is Dev UX focusing on in 13.0?

### Create Stage
* What the [Source Code Group](/handbook/product/categories/#source-code-group) is working on in 13.0
  * [FY21-Q2 IACV - UX: Improve the lovability of Merge Requests to refine the experience of IACV-driving product areas](https://gitlab.com/groups/gitlab-com/-/epics/443)
    * [KR: Prioritize the existing UX recommendations for Merge Requests](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7207)
    * [KR: Validate the current “Lovable” Category Maturity rating of Code Review with users](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7206)
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1116767?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Deliverable&label_name[]=UX&label_name[]=group%3A%3Asource%20code&milestone_title=13.0)
    * [Multi-line Merge Request MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/211255)
    * [Comment on Multiple Lines in Merge Request DIff](https://gitlab.com/gitlab-org/gitlab/-/issues/14128)
    * [Add to next and previous commit buttons when viewing commits in merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/18140)
* What the [Knowledge Group](/handbook/product/categories/#knowledge-group) is working on in 13.0
  * [Video update](https://youtu.be/x26KzqdwmSI) `5m40s`
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1122530?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aknowledge)
    * [Allow annotations to be resolvable](https://gitlab.com/gitlab-org/gitlab/-/issues/13049)
    * [Make the design collection more visible in the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/14744)
    * [Reflect Wiki's deep nesting in the structure sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17673)
    * [Design Reviews - Solution Validation](https://gitlab.com/gitlab-org/ux-research/-/issues/688)
    * [Designs showing up in the Review App - Solution Validation](https://gitlab.com/gitlab-org/gitlab/-/issues/198092)
    * [Show designs in a browser extension - Solution Validation](https://gitlab.com/gitlab-org/ux-research/-/issues/589)
* What the [Ecosystem Group](/handbook/product/categories/#ecosystem-group) is working on in 13.0
  * [Video Update](https://youtu.be/cJbjBKS1gJI) `10m39s`
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1167634?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=UX)
    * [Mass Integration at a Group and Instance Level](https://gitlab.com/groups/gitlab-org/-/epics/2137)
    * [Migrate Existing Service Template Instance Integration](https://gitlab.com/gitlab-org/gitlab/-/issues/204804)
    * [End-to-End mockup of mass integrations](https://gitlab.com/gitlab-org/gitlab/-/issues/208008)
    * [Making JIRA transition IDs easier to configure](https://gitlab.com/gitlab-org/gitlab/-/issues/16119)
      * [Video Walkthrough](https://youtu.be/cJbjBKS1gJI?t=442)
    * [Figma Pilot](https://gitlab.com/gitlab-org/gitlab-ui/-/issues/756)
    * [Overview for Projects with enabled specific service](https://gitlab.com/gitlab-org/gitlab/-/issues/14157)
      * > As a customer, I want to know many projects have a specific service enabled.
* What the [Static Site Editor Group](/handbook/product/categories/#static-site-editor-group) is working on in 13.0
  * [Video Update](https://youtu.be/-lroimS5NSw) `2m29s`
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1476633?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Astatic%20site%20editor&label_name[]=UX&milestone_title=13.0)
    * [Static Site Editor MVC Solution Validation](https://gitlab.com/gitlab-org/create-stage/-/issues/12673)
    * [Static Site Editor Solution Validation: WYSIWYG Editor](https://gitlab.com/gitlab-org/create-stage/-/issues/12674)
    * [Video Walkthrough of WYSIWYG](https://www.youtube.com/watch?v=sH7oSmwtmUQ&feature=youtu.be)
* What the [Editor Group](/handbook/product/categories/#editor-group) is working on in 13.0
  * [Video Update](https://youtu.be/wXi-3asYU0g) `3m50s`
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1124143?&label_name[]=group%3A%3Aeditor)
    * [Dark theme for Web IDE sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/213826)
    * [Fix markdown preview styling](https://gitlab.com/gitlab-org/gitlab/-/issues/214239)
      * > Opportunity for cross-group collaboration with Static Site Editor Group!
    * [Dark theme for modals in the Web IDE](https://gitlab.com/gitlab-org/gitlab/-/issues/213828)
      * > NOT planned for 13.0 (in the backlog)
    * [Web IDE shows warning message when user opens IDE and can't push code](https://gitlab.com/gitlab-org/gitlab/-/issues/213581)
    * [Reduce Web IDE Upload file icon user confusion](https://gitlab.com/gitlab-org/gitlab/-/issues/29203)

### Manage Stage
* What the [Import Group](/handbook/product/categories/#import-group) is working on in 13.0
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1344387?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX&label_name[]=devops%3A%3Amanage&milestone_title=13.0&label_name[]=group%3A%3Aimport)
    * [Better UX for Project Creation](https://gitlab.com/gitlab-org/gitlab/-/issues/210599)
* What the [Access Group](/handbook/product/categories/#access-group) is working on in 13.0
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1344387?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX&milestone_title=13.0&label_name[]=group%3A%3Aaccess&label_name[]=devops%3A%3Amanage)
    * [Group Managed Accounts - Group Owner Experience](https://gitlab.com/gitlab-org/gitlab/-/issues/214355)
    * [Group Managed Accounts - Group Member Experience](https://gitlab.com/gitlab-org/gitlab/-/issues/214352)
* What the [Analytics Group](/handbook/product/categories/#analytics-group) is working on in 13.0
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1244106?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aanalytics&milestone_title=13.0&label_name[]=UX)
  * Research
    * [Creating research plan for To-Do metrics](https://gitlab.com/gitlab-org/ux-research/-/issues/838)
  * Feature design
    * [Adding metrics to To-Do list](https://gitlab.com/gitlab-org/gitlab/-/issues/214284)
    * [VSA: Add controls for adding/editing removing stages to path navigation](https://gitlab.com/gitlab-org/gitlab/-/issues/216227)
    * [General metrics API (for insights) vision mockups](https://gitlab.com/groups/gitlab-org/-/epics/3170)
  * Pajamas
    * Report Object
    * [Add Table component updates](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1127)
    * [Create Filter region](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1123)
    * [Create Analytics page header region](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1124)
* What the [Compliance Group](/handbook/product/categories/#compliance-group) is working on in 13.0
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1344387?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX&label_name[]=devops%3A%3Amanage&label_name[]=group%3A%3Acompliance)
    * [Allow admins to scope admin-level MR approval settings to compliance-labeled projects](https://gitlab.com/gitlab-org/gitlab/-/issues/213601)
    * [Discovery: Two-person access controls for sensitive project settings](https://gitlab.com/gitlab-org/gitlab/-/issues/33260)
    * [Discovery: Allow MRs to poll external services as a criteria for approval](https://gitlab.com/gitlab-org/gitlab/-/issues/196115)
    * [Discovery: Allow admins to define Compliance rules within GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/211842)

### Plan Stage
* What the [Certify Group](/handbook/product/categories/#certify-group) is working on in 13.0
  * [Issue Board]()
    * [Add issue participants via email address](https://gitlab.com/gitlab-org/gitlab/-/issues/195525)
    * [Display actual dependencies on Issue Board](https://gitlab.com/gitlab-org/gitlab/-/issues/210452)
    * [UI for browsing requirements tests results](https://gitlab.com/gitlab-org/gitlab/-/issues/215516)
    * [Requirements Documentation MVC - Usability Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/627)
* What the [Portfolio Management Group](/handbook/product/categories/#portfolio-management-group) is working on in 13.0
  * [Video update](https://www.youtube.com/watch?v=Or5x5urxuLs) `11m39s`
  * [Release Planning Issue](https://gitlab.com/gitlab-org/plan/-/issues/85)
    * [Epic Swimlanes](https://gitlab.com/gitlab-org/gitlab/-/issues/7371)
    * [Metadata Organization](https://gitlab.com/gitlab-org/gitlab/-/issues/215042)
    * [Metadata UI Polish](https://gitlab.com/gitlab-org/gitlab/-/issues/216202)
    * [Reduce Epic Health Status "Noise" in Epic tree when no statuses are assigned](https://gitlab.com/gitlab-org/gitlab/-/issues/214675)
    * [Show epic information in issue list](https://gitlab.com/gitlab-org/gitlab/-/issues/10482)
    * [Customer Research and Validation Roadmapping Needs and Current State](https://gitlab.com/gitlab-org/gitlab/-/issues/215722)
    * Various roadmap performance and cleanup issues to eventually get to [Q1 OKR: Validate Roadmaps Maturity Level with Users => 0%](https://gitlab.com/gitlab-org/ux-research/-/issues/784)
* What the [Project Management Group](/handbook/product/categories/#project-management-group) is working on in 13.0
  * [FY21-Q2 IACV - UX: Improve the lovability of Issues to refine the experience of IACV-driving product areas](https://gitlab.com/groups/gitlab-com/-/epics/444)
    * [KR: Refine and add severity to existing Issue Tracking recommendations](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7210)
    * [KR: Validate the current “Complete” Category Maturity rating of Issue Tracking with users](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7209)
  * [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1285239?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aproject%20management&milestone_title=13.0&label_name[]=UX)
    * [Manage iteration and release cadence](https://gitlab.com/groups/gitlab-org/-/epics/69)
    * [Iterations in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/2422)
    * [Track scope change within a milestone](https://gitlab.com/groups/gitlab-org/-/epics/1957)
    * [JIRA import research](https://gitlab.com/gitlab-org/ux-research/-/issues/771)
