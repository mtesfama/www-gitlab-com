---
layout: markdown_page
title: "TMRG - GitLab Women"
description: "An overview of our remote TMRG GitLab Women"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-women/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction



## Mission

The mission of the GitLab Women TMRG is to cultivate an inclusive environment that supports and encourages women to advance their skills and leadership potential through connection, mentorship, collaboration and discussion.  This group shall serve as a forum for women to find their voice and be heard within the GitLab community. Through networking, socializing, and professional development we hope to attract and retain women into GitLab’s positions. This group is open to all members of the GitLab community. 

## Leads
* [Kyla Gradin](https://about.gitlab.com/company/team/#kyla)
* [Madeline Hennessy](https://about.gitlab.com/company/team/) - Co Lead 


## Executive Sponsors
* [Robin Schulman](https://about.gitlab.com/company/team/#rschulman) - Chief Legal Officer and Corporate Secretary
* [Michael McBride](https://about.gitlab.com/company/team/#mmcb) - Chief Revenue Officer

## How To Get Involved
* Please sign up [here](https://docs.google.com/forms/d/1HGYvm8MZ83cN7WAflY4Y6kOehPUUtBk_04cNFt2qtKQ/edit). Once the form is complete you will be added to our monthly meetings. If you have additional questions on how to particpate please reach out Kyla or Madeline to find out more.


## Upcoming Events 
* Women on the Move Blog - More to come!


## Related Performance Indicators and Goals

* [Women at GitLab](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-at-gitlab)
* [GitLab Women in Management - 30% Goal](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-in-management)
* [GitLab Women in Senior Leadership and Executive Roles](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-in-senior-leadership-and-executive-roles)

## Career Development Resources

Women at GitLab lookging for career development resources can use this list to explore both opportunities available to the entire GitLab team for career development and helpful resources that havd been shared in the [#women Slack channel](https://app.slack.com/client/T02592416/C7V402V8X) 

If you have a resource you'd like to include, please open a merge request and contribute to this page!

| Resource | Description |
| ----- | ----- | 
| [GitLab Internship for Learning](/handbook/people-group/promotions-transfers/#internship-for-learning) | If your manager has coverage, you can spend a percentage of your time working (through an 'internship') with another team |
| [CEO Shadow Program](/handbook/ceo/shadow/)| The goal of the CEO Shadow Program is to give current and future directors and senior leaders at GitLab an overview of all aspects of the company |
| [Women in the Workplace Study Webinar](https://www.hracuity.com/webinar/leaninstudy-2020?utm_campaign=Content%20%7C%20Webinar%20%7C%20LeanIn&utm_medium=email&_hsmi=98577248&_hsenc=p2ANqtz-8kbiJWLM1fyvHgX1E15FQtDxjVDla2B4C3p2G7wSvpUQMJizBBdpFhEpmhEdG7PeYR-ekzWWPWhWK9EfpXoZwk5m6dWQ&utm_content=98577248&utm_source=hs_email) | A webinar from Lean In discussing the unique impact that COVID-19 has had on women of different races and ethnicities, working mothers, women in senior leadership and women with disabilities |
| [The Harvard Gazette - Women less included to self-promote than men, even for a job](https://news.harvard.edu/gazette/story/2020/02/men-better-than-women-at-self-promotion-on-job-leading-to-inequities/) | Study finds female workers’ deep discomfort over touting skills, experience adds to gender gap in promotions, pay |


## Book Suggestions

The following are books suggested by GitLab team members in the [#women Slack channel](https://app.slack.com/client/T02592416/C7V402V8X). Please open an merge request to add books to this page, and consider including a short review of the book from your perspective! 

| Book Title and Author | Optional Book Review |
| ----- | ----- |
| [WolfPack by Abby Wambach](http://abbywambach.com/books/wolfpack/) | |
| [Radical Candor by Kim Scott](https://www.radicalcandor.com/the-book/) | |
| [Lean In by Sheryl Sandberg](https://leanin.org/book) | |
| [The Memo by Minda Harts](https://www.mindaharts.com/book/) | |
| [Burnout - The Secret to Unlocking the Stress Cycle by Amelia Nagoski and Emily Nagoski](https://bookshop.org/books/burnout-the-secret-to-unlocking-the-stress-cycle/9781984818324) | |


## Additional Resources

