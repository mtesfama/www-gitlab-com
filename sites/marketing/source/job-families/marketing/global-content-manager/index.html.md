---
layout: job_family_page
title: "Senior Manager, Global Content"
---

As the lead of the global content team you will manage the team responsible for content strategy, development, and governance. 
This role will drive audience, prospect, and customer interaction through thoughtful multimedia storytelling and collaborate cross-functionally to identify content needs, strategy, and process. The qualified candidate will be a motivated and engaged senior manager with a successful record of guiding and building content programs and teams globally. Reporting to the Senior Director, Corporate Marketing, the Senior Manager, Global Content must have experience across editorial, content marketing, and video production.

### Job Grade 

The Senior Manager, Global Content is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Define GitLab's global content strategy 
- Act as a steward of the GitLab brand ensuring quality and cohesiveness across all content development 
- Oversee global content creation, governance, and operations 
- Overseeing and launching content marketing programs
- Effectively and efficiently partner with brand & digital, events, and product marketing to deliver on content needs across the customer lifecycle
- Manage & mentor the content marketing, digital production, and editorial teams
- Generate and implement process improvements to the content pillar model
- Draft quarterly OKRs and content KPIs
- Report on content performance and audience growth
- Use data to measure results and inform planning and strategy development 
- Develop a localization strategy for marketing content 
- Define agile content process definition and application 
- Content strategy & inbound content marketing program for about.gitlab.com

### Requirements

- 5+ years experience in content marketing & creation, preferably in the software industry
- Excellent writer and editor 
- Excellent verbal communication skills 
- Experience building a content pillar framework
- Proven track record of developing and executing a successful content strategy 
- Credibility in craft: Past experience as a content marketer and managing content teams 
- Excellent at cross-functional collaboration 
- Dual minded: ability to focus on process and creative 
- Experience as a hiring manager
- An exemplary people manager 
- Experience managing managers
- Ability to successfully manage at a remote-only company
- Humble, servant leader

### Nice-to-have requirements

- Be a user of GitLab and familiarity with our company
- Experience creating content within the area of application development
- Technical background or solid understanding of developer products; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus
- Experience working with global teams
- Prior high-growth start up experience 

## Career Ladder

The next step in the Senior Manager, Global Content job family is not yet defined at GitLab. 

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Director, Corporate Marketing 
* A 45 minute interview with our Senior Manager, Corporate Communications 
* A 45 minute interview with our Director, Brand & Digital
* A 45 minute interview with our CMO 
* Finally, our CEO may choose to conduct a final interview
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
