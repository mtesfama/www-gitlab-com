---
layout: markdown_page
title: "How a continuous integration pipeline works"
description: "Pipelines are fundamental to continuous integration. Learn how continuous integration pipelines work and how they automate code tests and builds."
---
## What is a CI/CD pipeline?

Pipelines are a basic component of both [continuous integration (CI) and continuous delivery/deployment (CD)](/topics/ci-cd/). Pipelines ensure that only code that meets certain quality standards makes it into production. CI pipelines make sure that all code goes through the same process and automates tests and builds. Before learning how pipelines work, it’s important know [relevant terms](/blog/2019/07/12/guide-to-ci-cd-pipelines/).


## CI/CD pipeline glossary

**Commit:** A code change.

**Job:** Instructions for the agent or runner to execute.

**Pipeline:** A collection of jobs that are divided by stages

**Runner:** An agent or server that executes each job.

**Stages:** A keyword that defines phases of a pipeline, such as build and deploy. Jobs within a stage are executed in parallel.


## CI/CD pipeline phases & steps

A continuous integration pipeline consists of two things:

1. Jobs, which define _what_ to do. For example, jobs that compile or test code.
2. Stages, which define _when_ to run the jobs. For example, stages that run tests after stages that compile the code.

In GitLab, CI pipelines are configured using a version-controlled YAML file, `.gitlab-ci.yml`, within the root of a project. From there, you can set up parameters of your pipeline:

*   What to execute using [GitLab runner](https://docs.gitlab.com/ee/ci/runners/#configuring-gitlab-runners)
*   What happens when a process succeeds or fails

As code goes through each stage of the development process, it’s continually validated against other changes <a href="https://martinfowler.com/articles/branching-patterns.html" target="_blank">happening concurrently</a> in the shared repository. 

Here is a simple continuous integration pipeline diagram:

![simple ci pipeline diagram](https://docs.gitlab.com/ee/ci/pipelines/img/pipelines.png)

If _all_ jobs in a stage succeed, the pipeline moves on to the next stage.

If any job in a stage fails, the pipeline (usually) ends early before it proceeds to the next stage. Continuous integration pipeline activities can be customized to meet your unique needs.

Not all jobs are so simple. For larger products that require cross-project interdependencies, such as those adopting a [microservices architecture](/blog/2019/06/17/strategies-microservices-architecture/), there are [multi-project pipelines](/blog/2018/10/31/use-multiproject-pipelines-with-gitlab-cicd/).

![multi-project ci pipeline diagram](/images/9_3/multi-project_pipelines.png)


## How CI/CD pipelines support DevOps

A continuous integration pipeline improves code quality by ensuring that all code changes go through the same process. Code changes are validated against other changes being committed to the same shared code repository. Automated tests and builds decrease the chance of human error, creating faster iterations and better quality code.

## More about optimized continuous integration

[Scaled continuous integration and delivery](/resources/scaled-ci-cd/)

[Mastering continuous software development](/webcast/mastering-ci-cd/)

[Modernize your CI/CD](/resources/ebook-fuel-growth-cicd/)
