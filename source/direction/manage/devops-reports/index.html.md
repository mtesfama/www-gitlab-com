---
layout: markdown_page
title: "Category Direction - DevOps Reports"
canonical_path: "/direction/manage/devops-reports/"
---

- TOC
{:toc}

## Manage Stage

| Category | **DevOps Reports** |
| --- | --- |
| Stage | [Manage](https://about.gitlab.com/handbook/product/categories/#manage-stage) |
| Group | [Analytics](https://about.gitlab.com/handbook/product/categories/#analytics-group) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-08-06` |

### Introduction and how you can help
Thanks for visiting the direction page for DevOps Reports in GitLab. This page is being actively maintained by the Product Manager for the [Analytics group](https://about.gitlab.com/handbook/product/categories/#analytics-group). If you'd like to contribute or provide feedback on our category direction, you can:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/3651).
1. Find issues in this category accepting merge requests.

### Overview

GitLab has a [extraordinary breadth](https://about.gitlab.com/direction/maturity/#category-maturity) for a single application, allowing users to implement the entire [DevOps Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) with ten stages of functionality ranging from portfolio planning and management; on the one hand, all the way to monitoring and service desk on the other. Each of these stages offers persona specific dashboards such our [security dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), a dashboard for [monitoring](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#getting-metrics-to-display-on-the-metrics-dashboard) applications, for [auditors and compliance officers](https://docs.gitlab.com/ee/user/compliance/compliance_dashboard/index.html#compliance-dashboard), for engineers tracking the progress of [code reviews](https://docs.gitlab.com/ee/user/analytics/code_review_analytics.html) their software teams.

With this breadth, GitLab has the potential to provide a level of insight that few others can match. To help users more fully realize this potential we want to address one important view we currently lack: the ability to pull all of these disparate pieces together into a single comprehensive view that cuts across stage boundaries.

The DevOps Reports category aims to address this by providing users the ability to aggregate data across all of GitLab in useful ways, drill-in for additional detail and analysis, and contextualize performance indicators by identifying areas of opportunity.

#### Themes

* **Optimizing Flow** &mdash; In conjunction with our emphasis on [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/), we will implement common DevOps and Value Stream metrics such as the [DORA](https://gitlab.com/gitlab-org/gitlab/issues/37139) metrics and Flow Metrics in addition to general value stream process measures for evaluating the performance of a particular value stream process step or stage, detecting out-of-control queues, process bottlenecks, etc.

* **Planning & Tracking** &mdash; We aim to assist agile project managers and agile teams with frequently used measures such as team velocity, cumulative flow and eventually predictive analytics to help with [planning](https://about.gitlab.com/direction/manage/planning-analytics/).

* **Develop & Test, Secure & Release** &mdash; Development teams often wish to monitor status of builds, test code coverage, security scan results, defect counts and other measures which are also used by Release Managers to gauge release readiness.

* **Measures of Value** &mdash; Our existing Prometheus integration provides a powerful capability for tracking the outcome of a continuous delivery release in terms of [business metrics that matter](https://www.youtube.com/watch?time_continue=1&v=oG0VESUOFAI&feature=emb_logo). We hope to build on this with deeper integration to our system for [feature flags](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) and [a/b testing](https://gitlab.com/groups/gitlab-org/-/epics/2966).

* **DevOps Maturity** &mdash; We will begin with [relatively straightforward](https://gitlab.com/gitlab-org/gitlab/issues/193435) measurements of usage and adoption as an evolution of our [DevOps Score](https://docs.gitlab.com/ee/user/instance_statistics/dev_ops_score.html) capability. Eventually leaders will be able to set and specific KPIs and process targets derived from out-of-the box Metrics Library metrics on an executive dashboard.

### What's Next and Why
Specific near term steps in this area include:

* A simple [instance-level overview](https://gitlab.com/groups/gitlab-org/-/epics/4147) of GitLab activity available to all instances.
* A more complex [DevOps Report](https://gitlab.com/gitlab-org/gitlab/-/issues/232723) in Ultimate that allows an instance to create custom segments of projects and groups and compare KPIs across segments.

### Maturity Plan
This category is currently **Minimal**. The next step in our maturity plan is achieving a **Viable** state of maturity.
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
