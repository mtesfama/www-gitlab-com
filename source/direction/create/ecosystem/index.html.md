---
layout: markdown_page
title: "Group Direction - Ecosystem"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Create](/direction/dev/#create-1) | 
| **Last reviewed** | 2020-07-28 |

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem)

## Overview

Ecosystem's mission is to foster both GitLab as a platform as well as its community.
Ecosystem supports our **Integrations** with other products, our **APIs** for
connecting GitLab to external products and services, and our **GDK** and
**Frontend Foundations** that enable our community of contributors to 
to develop the application itself.

Our goal is to make integrating with, extending the functionality of, or 
developing GitLab itself an easy, delightful experience. As with all things 
GitLab, we strongly believe that [Everyone can contribute](#contributing), and 
Ecosystem supports that mission directly through its work.

## Categories

### Integrations

Integrations are places GitLab connects to features and services from other products,
_directly from the GitLab codebase_. These integrations
range from things like lightweight [Slack notifications](https://docs.gitlab.com/ee/user/project/integrations/slack.html) 
for projects, to complex integrations with [Atlassian Jira](https://docs.gitlab.com/ee/user/project/integrations/jira.html).

Today, there are several ways to integrate into GitLab&mdash;by adding your
integration to the [GitLab codebase](https://gitlab.com/gitlab-org/gitlab), by consuming our [public APIs](https://docs.gitlab.com/ee/api/), or by
using a community library to connect your code to GitLab. Many products,
tools, and services that have already integrated with GitLab can be found on
our [partner integration page](https://about.gitlab.com/partners/integrate/).

Integrations will focus primarily on adding new integrations that are 
_key to the needs of our enterprise customers_ and providing guidance for 3rd parties that are 
contributing integrations for their own products.

[Category Direction](/direction/create/ecosystem/integrations/) &middot; 
[Documentation](https://docs.gitlab.com/ee/integration/) &middot; 
[Epic](https://gitlab.com/groups/gitlab-org/-/epics/1515) &middot; 
[Open Issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations)

### API

The GitLab APIs enable external products and services to access GitLab data and functionality.

The GitLab API **category** provides guidance and governance for all the
groups inside of GitLab that are creating and maintaining our REST and GraphQL
APIs. The goal of this effort is to define best practices and requirements
for the development of our APIs to create a consistently great experience for
those integrating with GitLab as a platform.

[Category Direction](/direction/create/ecosystem/api/) &middot; 
[Documentation](https://docs.gitlab.com/ee/api/) &middot; 
[Epic](https://gitlab.com/groups/gitlab-org/-/epics/2040) &middot; 
[Open Issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs)

### GDK

The GitLab Development Kit (GDK) is a tool for developers contributing
to GitLab. It provides a simple way to install, configure, and run a local
development environment. This tool is key to the success of our contributor
community&mdash;both internal GitLab Team Members and the broader community of
contributors.

[Category Direction](/direction/create/ecosystem/gdk) &middot; 
[Project Home](https://gitlab.com/gitlab-org/gitlab-development-kit/) &middot; 
[Setup Documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/set-up-gdk.md) &middot; 
[Open Issues](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues)

### FE/UX Foundations

The work of Frontend and UX Foundations centers around resources and tools that
allow Frontend Engineers and Product Designers to design and implement the UI of
GitLab more effectively. This includes the [Pajamas Design System](https://design.gitlab.com/),
and frontend tooling such as our webpack implementation. The goal of these
efforts are to make developing the GitLab UI straightforward, performant,
and maintainable.

[Category Direction](/direction/create/ecosystem/frontend-ux-foundations) &middot;
[Pajamas Documentation](https://design.gitlab.com/)

## Ecosystem Themes

### Freedom of choice

We firmly believe that a [single application](https://about.gitlab.com/handbook/product/single-application/) 
can successfully serve all the needs of the entire DevOps lifecycle. However, there are a myriad
of reasons that many customers can't adopt GitLab in this way.

Customers may have specific tools they are committed to using due to factors like:

1. The cost of migrating off of it, either based on the volume of content to 
  migrate, the risk of errors during the migration, the cost of training, etc.
1. The cost of building _new integrations_ with other tools in their existing toolchain
1. Specific regulatory, security, or compliance needs they must be able to meet
1. Niche or unique functionality that isn't available in GitLab

Because of these realities, we believe that our customers should have the 
**freedom to choose their tools**, and use what makes the most sense for their 
business&mdash;and we will support that freedom as best we can by [playing well with others](https://about.gitlab.com/handbook/product/gitlab-the-product/#plays-well-with-others).

### Flexibility and Extensibility

We'll never anticipate every possible use-case, nor can we afford to support the 
development of every possible integration. So to that end, our aim is to create 
flexible and extensible tools so that those integrating with us can create whatever they need. 

## Further Reading

### Problem/Solution Validation

We work hard to understand the space we operate in, and to better understand our
customers and the problems we're solving for them, we conduct [Validation exercises](https://about.gitlab.com/handbook/product-development-flow/#validation-track).

* [ServiceNow Problem Validation](https://gitlab.com/gitlab-org/ux-research/-/issues/518)
* [Solution Validation: Mass Integrations](https://gitlab.com/gitlab-org/ux-research/-/issues/789)
* [Integration Settings Prototype Testing](https://gitlab.com/gitlab-org/ux-research/-/issues/601)
* [Understanding our developer experience](https://gitlab.com/gitlab-org/ux-research/-/issues/511)
* [Understanding group-level integration with Jira](https://gitlab.com/gitlab-org/ux-research/-/issues/393)

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking
to contribute your own integration or otherwise get involved with features in
the Ecosystem area, [you can find open issues here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem).

Feel free to reach out to the team directly if you need guidance or want
feedback on your work by pinging [@deuley](https://gitlab.com/deuley) or
[@gitlab-org/ecosystem-team](https://gitlab.com/gitlab-org/ecosystem-team) on your
open MR.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Influences

We're inspired by other companies with rich, developer-friendly experiences like 
[Salesforce](https://developer.salesforce.com/), 
[Shopify](https://help.shopify.com/en/api/getting-started), 
[Twilio](https://www.twilio.com/docs/), 
[Stripe](https://stripe.com/docs/development), 
and [GitHub](https://developer.github.com/).

A large part of the success of these companies comes from their enthusiasm 
around enabling developers to integrate, extend, and interact with their 
services in new and novel ways, creating a spirit of [collaboration](https://about.gitlab.com/handbook/values/#collaboration) 
and [diversity](https://about.gitlab.com/handbook/values/#collaboration) that 
simply can't exist any other way. 
